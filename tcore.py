"""
Core part of timer.

@author: Tang142857
@project: SchoolSupport
@file: core.py
@date: 2021-02-21
Copyright(c): DFSA Software Develop Center
"""
import threading
import time


class ArgumentPackage(object):
    pass


class Event(object):
    """
    Stronger event object ,it allow you to add args,and pass to callbacks with event...
    Now ,you can pass arguments even emit the event.
    But pay attention ,all arguments will be packed in event_args and
    the arguments went emit and not key-value will in emit_args
    """

    def __init__(self, **args):
        self.callback_functions = []
        self.event_args = ArgumentPackage()
        for arg in args:  # bind arguments
            setattr(self.event_args, arg, args[arg])

    def add_callback(self, func):
        assert callable(func), 'ERROR_IS_NOT_CALLABLE'
        self.callback_functions.append(func)

    def emit(self, *arg, **args):
        # TODO async rewrite
        for argument in arg:
            setattr(self.event_args, 'emit_args', argument)
            # you will find only the last argument can be remain
            # so please avoid use list-arguments as possible as you can
        for argument in args:
            setattr(self.event_args, argument, args[argument])
        self._call()

    def _call(self):
        for cell in self.callback_functions:
            cell(self.event_args)


class TimeCore(object):
    """
    核心时间显示
    放弃复杂的位置算法，采用整体递减后格式化的方式
    """
    lock_var = False

    def __init__(self, intTine=10):
        # int time between 0-356466
        self.full_number_time = intTine
        self.change_time_event = Event()

    def add(self, event=None, seconds=1):
        if self.lock_var:
            return
        if event is not None:
            seconds = event.change_by
        if 356466 >= self.full_number_time + seconds >= 0:
            self.full_number_time += seconds
            self.update()

    def subduction(self, event=None, seconds=1):
        # no need to lock subduction ,only signal sender use this
        if event is not None:
            seconds = event.change_by
        if 356466 >= self.full_number_time - seconds >= 0:
            self.full_number_time -= seconds
            self.update()
        else:
            return True  # to stop the signal sender

    def update(self):
        self.change_time_event.emit(int_time=self.full_number_time)

    def lock(self, event=None):
        self.lock_var = True

    def release(self, event=None):
        self.lock_var = False

    @staticmethod
    def translate(num_time):
        """Translate int time to time set"""
        hours, remain_time = num_time // 3600, num_time % 3600
        minutes, remain_time = remain_time // 60, remain_time % 60
        # str_time = str(hours).rjust(2, '0') + \
        #         ':' + str(minutes).rjust(2, '0') + \
        #         ':' + str(remain_time).rjust(2, '0')
        return hours, minutes, remain_time


class SignalSender(threading.Thread):
    RUN_FLAG = True

    def __init__(self, receiver, step: float):
        """
        secondlyCallback : function to call back,simply it is the subduction of time core.
        step : seconds step
        """
        super().__init__()
        self.step = step  # step length
        self.receiver = receiver
        self.end_event = Event()

        self.end_event.add_callback(self.reset)

    def run(self):
        """Override the run function to send signal."""
        self.RUN_FLAG = True
        # every time start ,change it to true ,because the signal sender only stop and start
        while self.RUN_FLAG:
            time.sleep(self.step)
            if self.receiver():  # end timing
                break
        return self.end_event.emit(status=self.RUN_FLAG)
        # when the run flag is true ,timing finished natural
        # when the run flag is false ,timing finished by stop

    def stop(self, event=None):
        self.RUN_FLAG = False

    def reset(self, event=None):
        """By calling Thread's init to solve can't continue problem."""
        super().__init__()


class Timer(object):
    """计时器"""

    def __init__(self):
        self.timing_core = TimeCore(3)
        self.signal_sender = SignalSender(self.timing_core.subduction, 1.0)

        self.signal_sender.end_event.add_callback(self.timing_core.release)

    def start(self):
        self.signal_sender.start()
        self.timing_core.lock()

    def stop(self):
        self.signal_sender.stop()

    def update(self):
        self.timing_core.update()


def _test(event):
    print(event.int_time)


if __name__ == '__main__':
    print(TimeCore.translate(123))
    t = Timer()
    t.timing_core.change_time_event.add_callback(_test)
    t.start()
    time.sleep(1)
    t.stop()
    time.sleep(1)
    print('line 136')
    t.start()
    # t.signal_sender.join()
    # time.sleep(10)
