Timer-tkinter
====
介绍：
----
![ui](resource/screenshoot.PNG)  
这是一个适用于Python3.8的桌面计时器，使用tkinter图形化模块，能力有限，持续更新  
执行入口：timer2  

#### 安装方法
1.Python3.7或者Python3.8  
3.克隆本仓库到本地  

```bash
git clone https://gitee.com/tang142857/timer.git
```

#### 运行原理

timer类包含脉冲器和计时核心，timer被startbutton和resetbutton调用接口，下发给脉冲器，脉冲器链接计时核心  
计时开始时调用脉冲器发送脉冲，暂停时退出脉冲并重置线程（reinitialize）  
重启时再次start脉冲器，只有计时归零时，脉冲器调用timer end函数结束计时  

#### 使用
按键字面义即可  

#### 已知的问题
Had solved 1.暂停后重启再次start线程时RuntimeError: threads can only be started once  
Had solved 2.失常的运行时set函数  

#### 更新

2021年1月10日15:46:56tkinter版timer创建  
2021年1月17日14:39:49单向计时功能完成  
2021年1月21日20:51:37修复重启，退出问题，维护readme  
