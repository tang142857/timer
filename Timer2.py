"""
Simple timer for school.

@author: Tang142857
@project: SchoolSupport
@file: Timer2.py
@date: 2021-02-21
Copyright(c): DFSA Software Develop Center
"""

import os
import time
import tkinter
import tkinter.messagebox

import playsound

import tkui
import tcore


class Timer(object):
    STATUS_CODE = 1

    # 1:not run and not stop
    # 2:started and not stop
    # s:started and stop
    def __init__(self):
        self.timer_core = tcore.Timer()
        self.timer_core.signal_sender.end_event.add_callback(self.end)

        UI_WIDGETS.time_displayer.value_change_event.add_callback(self.timer_core.timing_core.add)
        self.timer_core.timing_core.change_time_event.add_callback(UI_WIDGETS.time_displayer.set_time)
        # bind displayer and timing core end

        UI_WIDGETS.start_event.add_callback(self.startButtonCall)
        UI_WIDGETS.reset_event.add_callback(self.resetButtonCall)
        # bind ui and manage function end

    def startButtonCall(self, event=None):
        """Even through it is start button ,it also need to be the stop button :) """
        printStatus(f'call start ,status code:{self.STATUS_CODE}')
        # self.timer_core.start()
        if self.STATUS_CODE == 1:  # to start timing
            self.timer_core.start()
            UI_WIDGETS.startButton.config(text='stop')
            self.STATUS_CODE = 2
        elif self.STATUS_CODE == 2:  # to stop
            self.timer_core.stop()
            UI_WIDGETS.startButton.config(text='continue')
            self.STATUS_CODE = 3
        elif self.STATUS_CODE == 3:  # to continue
            self.timer_core.start()
            UI_WIDGETS.startButton.config(text='stop')
            self.STATUS_CODE = 2

    def resetButtonCall(self, event=None):
        printStatus(f'call reset ,status code:{self.STATUS_CODE}')
        DevelopError()  # TODO reset button ,and save start configure

    def end(self, event=None):
        """End timing and play sounds"""
        assert event is not None, 'CAN_NOT_CONNECT_SIGNAL_SENDER Timer.end'

        if event.status:
            playsound.playsound('resource/ring.mp3')
            UI_WIDGETS.startButton.config(text='start')
            self.STATUS_CODE = 1
        else:  # user press stop
            pass


def printStatus(values, end='\n', head=''):
    """
    Print the value with time string.
    :param head: sting head just like \\r
    :param end: string end just like \\n
    :param values: value
    :return: nothing
    """
    origin_time = time.localtime(time.time())  # 创建本地时间
    time_string = time.strftime('%H:%M:%S', origin_time)  # 生成时间戳
    print(f'{head}INFO:{time_string} :: {values.__str__()}', end=end)


def DevelopError():
    tkinter.messagebox.showwarning('Develop Error',
                                   'This function has not been developed,please wait for a short time.\n此功能尚未开发，请稍候。')


def getExited():
    """Using inside pack to solve running can't exit problem."""

    def myExit():  # 这是一个闭包，用于解决运行时无法退出的问题（signal sender停不下来）
        os._exit(0)

    return myExit


if __name__ == "__main__":
    MAIN_WINDOW = tkinter.Tk()  # Create main window.
    UI_WIDGETS = tkui.MainWidgets(MAIN_WINDOW)
    UI_WIDGETS.setupUi()
    # UI config end

    TIMER = Timer()

    # MAIN_WINDOW.bind('<Configure>', printStatus)
    MAIN_WINDOW.protocol('WM_DELETE_WINDOW', getExited())  # to get the packed function ,you should call it

    TIMER.timer_core.update()
    MAIN_WINDOW.mainloop()
