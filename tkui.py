"""
User interface for timer.

@author: Tang142857
@project: SchoolSupport
@file: tkui.py
@date: 2021-02-21
Copyright(c): DFSA Software Develop Center
"""
import tkinter
import sys

import tcore

MAIN_FONT = ('Microsoft YaHei', 14)
TIME_FONT = ('Microsoft YaHei', 120)


class TimeDisplayer(tkinter.Frame):
    """
    时间显示器，显示格式 HH:MM:SS
    提供刷新方法，时间设置事件
    """
    def __init__(self, master, **timer_arg):
        super().__init__(master)
        self._hours = tkinter.Label(self, font=TIME_FONT, text='01')
        self._minutes = tkinter.Label(self, font=TIME_FONT, text='02')
        self._seconds = tkinter.Label(self, font=TIME_FONT, text='03')

        self._split_a = tkinter.Label(self, font=TIME_FONT, text=':')
        self._split_b = tkinter.Label(self, font=TIME_FONT, text=':')

        self.value_change_event = tcore.Event()

        self._hours.bind('<Button-1>', self._update_mouse_position)
        self._minutes.bind('<Button-1>', self._update_mouse_position)
        self._seconds.bind('<Button-1>', self._update_mouse_position)
        # update when mouse button pushed

        self._hours.bind('<B1-Motion>', self._judge)
        self._minutes.bind('<B1-Motion>', self._judge)
        self._seconds.bind('<B1-Motion>', self._judge)

    def set_time(self, event=None, num_time=None):
        """Print a set of time (hour:int,minute:int,second:int) to displayer"""
        if num_time is not None:
            pass
        else:
            num_time = event.int_time

        displayers = [self._hours, self._minutes, self._seconds]
        for i, cell in enumerate(tcore.TimeCore.translate(num_time)):
            displayers[i].config(text=str(cell).rjust(2, '0'))

    def pack(self, **args):
        tkinter.Frame.pack(self, **args)
        self._hours.pack(side='left')
        self._split_a.pack(side='left')
        self._minutes.pack(side='left')
        self._split_b.pack(side='left')
        self._seconds.pack(side='left')

    # ####### public interface end here #######
    # I advise that you would better not see following codes
    # When I write them ,only God and me know ,but now ,only God

    def _judge(self, event=False):
        assert event, 'CAN_NOT_ACCESS_TO_UI TD._judge'
        widget_info = event.widget.__str__().split('!')[-1]

        if widget_info == 'label': position = 3600
        elif widget_info == 'label2': position = 60
        elif widget_info == 'label3': position = 1
        # pay attention here if widgets change ,please check the labels' names

        print(self.mouse_position,
              f'{event.widget}:{event.x}.{event.y} at position: {position}')

        if abs(event.y - self.mouse_position) > 20:
            if event.y > self.mouse_position:
                position = -position
            self.value_change_event.emit(change_by=position)
            self.mouse_position = event.y

    def _update_mouse_position(self, event=None):
        assert event, 'CAN_NOT_ACCESS_TO_UI TD._update_mouse_position'
        self.mouse_position = event.y


class MainWidgets(object):
    def __init__(self, mainWindow):
        self.mainWindow = mainWindow  # bind the main window to widgets
        self.start_event = tcore.Event()
        self.reset_event = tcore.Event()  # Wait for bind

    def setupUi(self):
        """setup ui for main window"""
        self.mainWindow.title('Timer-tkinter')
        if sys.platform == 'win32':
            self.mainWindow.iconbitmap('./resource/icon.ico')
        elif sys.platform == 'linux':
            print('Warning: Can not use icon file on linux.')
            pass  # FIXME can not use icon file on linux(ubuntu)
        self.mainWindow.resizable(False, False)
        # main window initialize end
        self.upsetFrame = tkinter.Frame(self.mainWindow)
        self.time_displayer = TimeDisplayer(self.mainWindow)
        self.downsetFrame = tkinter.Frame(self.mainWindow)
        self.settingFrame = tkinter.Frame(self.mainWindow)
        # Creat frames end

        self.startButton = tkinter.Button(self.settingFrame,
                                          text='start',
                                          font=MAIN_FONT,
                                          command=self.start_event.emit)
        self.resetButton = tkinter.Button(self.settingFrame,
                                          text='reset',
                                          font=MAIN_FONT,
                                          command=self.reset_event.emit)
        # Setting buttons create end

        self._packWidgets()

    def _packWidgets(self):
        """pack widget"""
        self.time_displayer.pack(expand=True, fill='both')
        self.settingFrame.pack(expand=True, fill='x')
        # Frames pack end

        self.startButton.pack(side='left', expand=True)
        self.resetButton.pack(side='right', expand=True)
        # Display and setting button pack end


def _test(event=None):
    print(event.change_by)


if __name__ == "__main__":
    testWindow = tkinter.Tk()
    UI = MainWidgets(testWindow)
    UI.setupUi()
    UI.time_displayer.set_time(num_time=321)
    UI.time_displayer.value_change_event.add_callback(_test)
    testWindow.mainloop()
